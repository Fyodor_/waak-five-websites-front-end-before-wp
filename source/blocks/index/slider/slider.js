jQuery(document).ready(function ($) {
  $('.slider-wrapper').slick({
    dots: true,
    arrows: true,
    appendDots: $('.dots-wrapper'),
    appendArrows: $('.arrows-wrapper'),
    autoplay: true,
    autoplaySpeed: 5000,
  });
});