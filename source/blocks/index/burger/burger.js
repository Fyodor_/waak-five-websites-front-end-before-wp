jQuery(document).ready(function ($) {
  $('.nav-icon4').click(function () {
    $(this).toggleClass('open');
  });
  $('.nav-icon4').click(function () {
    if ($(this).hasClass('open')) {
      $('#burgerDocuraPopup, #burgerDocuraPopupProduct').removeClass('hiddenone');
      setTimeout(function () {
        $('#burgerDocuraPopup, #burgerDocuraPopupProduct').removeClass('visuallyhiddenone');
        $('#burgerDocuraPopup, #burgerDocuraPopupProduct').addClass('opened');
      }, 50);
      $('body').css('overflow', 'hidden');
    } else {
      $('#burgerDocuraPopup, #burgerDocuraPopupProduct').addClass('visuallyhiddenone');
      $('#burgerDocuraPopup, #burgerDocuraPopupProduct').one('transitionend', function (e) {
        $('#burgerDocuraPopup, #burgerDocuraPopupProduct').addClass('hiddenone');
      });
      $('#burgerDocuraPopup, #burgerDocuraPopupProduct').removeClass('opened');
      $('.burger-content .producte').removeClass('open');
      $('.burger-content .producte + img').removeClass('openimg');
      $( ".burger-content .submenu-wrapper" ).css("display", "none");
      $('body').css('overflow', 'auto');
    }
  });
  $('.burger-content .producte').on('click', function(){
    if($('.burger-content .submenu-wrapper').css('display') === 'none'){
      $('.burger-content .producte').addClass('open');
      $('.burger-content .producte + img').addClass('openimg');
      $( ".burger-content .submenu-wrapper" ).slideToggle( "0.4s", function() {

      });
    } else {
      $('.burger-content .producte').removeClass('open');
      $('.burger-content .producte + img').removeClass('openimg');
      $( ".burger-content .submenu-wrapper" ).slideToggle( "0.4s", function() {

      });
    }

  });
});