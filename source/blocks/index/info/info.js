jQuery(document).ready(function ($) {
  //open some slide
  $('#accordion, #accordion2, #accordion3').on('show.bs.collapse', function (event) {
    $('#accordion h4').removeClass('open');
    $(event.target).parent().find('h4').addClass('open');
  });
  //close some slide
  $('#accordion, #accordion2, #accordion3').on('hide.bs.collapse', function (event) {
    $(event.target).parent().find('h4').removeClass('open');
  });
});